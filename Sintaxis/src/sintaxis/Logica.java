/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Calcula el promedio de los elementos de un arreglo
     *
     * @param arreglo Arreglo con los elementos
     * @return double con el promedio
     */
    public double promedio(int[] arreglo) {
        double promedio = 0;
        for (int i = 0; i < arreglo.length; i++) {
            promedio += arreglo[i];
        }
        return promedio / arreglo.length;
    }

    /**
     *
     * @param arreglo
     * @return
     */
    public int buscarMayor(int[] arreglo) {
        int mayor = 0;

        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] > mayor) {
                mayor = arreglo[i];
            }
        }

        return mayor;
    }

    /**
     * Suma los elementos impares de un arreglo
     *
     * @param arreglo Arreglo con los elementos
     * @return int con la suma de los números impares
     */
    public int sumaImpares(int[] arreglo) {
        int total = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                total += arreglo[i];
            }
        }
        return total;
    }

    /**
     * Busca un elemento dentro de un arreglo
     *
     * @param arreglo Arreglo con los datos
     * @param elemento Elemento que se busca
     * @return True en caso de encontrar el elemento, false caso contrario
     */
    public boolean buscarElemento(int[] arreglo, int elemento) {

        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento) {
                return true;
            }
        }

        return false;
    }

    /**
     * Busca un elemento y retorna la posicion del elemento
     *
     * @param arreglo Arreglo con los datos
     * @param elemento Elemento que se anda buscando
     * @return Posicion del elemento, -1 en caso de no encontrarse
     */
    public int buscarPos(int[] arreglo, int elemento) {

        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Invierte los elementos del arreglo
     *
     * @param arreglo Arreglo con los datos
     * @return Nuevo arreglo con los datos invertidos
     */
    public int[] invertir(int[] arreglo) {
        int[] resultado = new int[arreglo.length];
        int x = resultado.length - 1;
        for (int i = 0; i < arreglo.length; i++) {
            resultado[x] = arreglo[i];
            x--;
        }
        return resultado;
    }

    public void rotar(int[] arreglo) {
        int res = arreglo[arreglo.length - 1];
        for (int i = arreglo.length - 2; i >= 0; i--) {
            arreglo[i + 1] = arreglo[i];
        }
        arreglo[0] = res;
        //return arreglo;
    } 

    /**
     * Concatena en un string los valores del arreglo
     *
     * @param arreglo Arreglo con los datos
     * @return String con los valores del arreglo concatenados
     */
    public String imprimir(int[] arreglo) {
        String res = "";
        for (int i = 0; i < arreglo.length; i++) {
            res += arreglo[i] + ", ";
        }
        res += "\b\b";
        return res;
    }

}
