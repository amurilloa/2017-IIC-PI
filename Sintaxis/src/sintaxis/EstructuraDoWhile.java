/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraDoWhile {

    public static int leerInt(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        int n2 = sc.nextInt();
        return n2;
    }

    public static void main(String[] args) {
//        System.out.println("DO - WHILE");
//        int con = 11;
//        do {
//            System.out.println(con);
//            con++;
//        } while (con <= 10);
//
//        System.out.println("WHILE");
//        con = 11;
//        while (con <= 10) {
//            System.out.println(con);
//            con++;
//        }

        int edad = 0;
        int suma = 0;
        int contador = 0;
        double promedio = 0;
        do {
            edad = leerInt("Ingrese la edad de la persona");
            suma = suma + edad;
            contador = contador + 1;
            promedio = suma / contador;
            System.out.println("El promedio por ahora es " + promedio);
            if (promedio > 25) {
                break;
            }
        } while (edad >= 0);
    }
}
