/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class Tarea {

    public static String leerString(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        String n2 = sc.nextLine();
        return n2;
    }

    public static int leerInt(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        int n2 = sc.nextInt();
        return n2;
    }

    public static void main(String[] args) {
        int precio = leerInt("Digite el precio por persona del crucero");
        int cant = leerInt("Digite la cantidad de pasajeros");
        int total = precio * cant;
        //Cuando viajo solo 
        if (cant <= 0) {
            System.out.println("Cantidad inváilida de viajeros");
        } else if (cant == 1) {
            int edad = leerInt("Digite la edad del viajero");
            if (edad >= 18 && edad <= 30) {
                System.out.println("El precio a pagar es: " + (total * 0.922));
            } else if (edad > 30) {
                System.out.println("El precio a pagar es: " + (total * 0.90));
            } else {
                System.out.println("El precio a pagar es: " + total);
            }
        } else if (cant == 2) {
            System.out.println("El precio a pagar es: " + (total * 0.885));
        } else if (cant > 3) {
            System.out.println("El precio a pagar es: " + (total * 0.85));
        } else {
            System.out.println("El precio a pagar es: " + total);
        }
    }

}
