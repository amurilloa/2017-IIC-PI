/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraWhile {

    public static int leerInt(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        int n2 = sc.nextInt();
        return n2;
    }

    public static String leerTexto(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        String n2 = sc.nextLine();
        return n2;
    }

    public static void main(String[] args) {

        int tabla = leerInt("Digite el número de la tabla");
        int contador = 0;

        while (contador <= 9) {
            int resultado = contador * tabla;
            System.out.println(tabla + " x " + contador + " = " + resultado);
            contador++;
        }

        contador = 1;
        int sumatoria = 0;
        String numeros = "";

        while (contador <= 10) {
            int num = leerInt("Digite un número: ");
            sumatoria += num;
            if (contador == 10) {
                numeros += num + " = ";
            } else {
                numeros += num + " + ";
            }
            contador++;
        }

        System.out.println(numeros + sumatoria);

        String menu = "Menu\n"
                + "+ Sumar\n"
                + "- Restar\n"
                + "* Multiplicar\n"
                + "/ Dividir\n"
                + "x Salir\n"
                + "Seleccione una opción: ";
        etiqueta:
        while (true) {
            String opcion = leerTexto(menu);
            if (opcion.equals("x")) {
                break;
            }
            int n1 = leerInt("Numero 1");
            int n2 = leerInt("Numero 2");
            switch (opcion) {
                case "+":
                    System.out.println(n1 + n2);
                    break;
                case "-":
                    System.out.println(n1 - n2);
                    break;
                case "*":
                    System.out.println(n1 * n2);
                    break;
                case "/":
                    if (n2 == 0) {
                        System.out.println("Error!!");
                    } else {
                        System.out.println(n1 / n2);
                    }
                    break;
                case "x":
                    break etiqueta;
            }
        }
    }
}
