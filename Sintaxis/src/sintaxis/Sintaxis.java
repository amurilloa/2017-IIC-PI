/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Sintaxis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Hola Mundo");

        System.out.println("5==5: " + (5 == 5));
        System.out.println("5==6: " + (5 == 6));
        System.out.println("5!=6: " + (5 != 6));
        System.out.println("5>6: " + (5 > 6));
        System.out.println("5<=6: " + (5 <= 6));
        System.out.println("6<=6: " + (6 <= 6));

        System.out.println("Ejemplo de Booleans");

        boolean x = true;
        boolean y = false;
        System.out.println(x && y);

        x = true;
        y = true;

        System.out.println(x && y);

        String texto = JOptionPane.showInputDialog("Digite un número");
        int num = Integer.parseInt(texto);

        if (num % 2 == 0) {
            System.out.println("El número " + num + " es par");
        } else {
            System.out.println("El número " + num + " es impar");
        }
        int x1 = Integer.parseInt(JOptionPane.showInputDialog("Número 1"));
        int y1 = Integer.parseInt(JOptionPane.showInputDialog("Número 2"));

        if (x1 > y1) {
            System.out.println("X mayor que Y");
        } else if (y1 > x1) {
            System.out.println("Y mayor que X");
        } else {
            System.out.println("Son iguales");
        }

        int numero = Integer.parseInt(JOptionPane.showInputDialog("Digite un Número"));
        if (numero >= 0) {
            System.out.println("El número es positivo");
        } else {
            System.out.println("El número es negativo");
        }

        if (numero % 5 == 0) {
            System.out.println("El número es divisible entre 5");
        }
         
        int aleatorio = (int) (Math.random() * 6) + 1;
//        Random r = new Random();
//        aleatorio = r.nextInt(6)+1;
        System.out.println(aleatorio);
         

        int numEjes = Integer.parseInt(JOptionPane.showInputDialog("Número de ejes"));
        int canPasa = Integer.parseInt(JOptionPane.showInputDialog("Número Pasajeros"));
        double costoVehi = Integer.parseInt(JOptionPane.showInputDialog("Costo base del vehículo"));
        double impuesto = costoVehi * 0.01; //1% del costo del vehiculo 
        double costoTotal = costoVehi + impuesto;

        if (canPasa < 20) {
            costoTotal += impuesto * 0.01;
        } else if (canPasa >= 20 && canPasa <= 60) {
            costoTotal += impuesto * 0.05;
        } else {
            costoTotal += impuesto * 0.08;
        }

        if (numEjes == 2) {
            costoTotal += impuesto * 0.05;
        } else if (numEjes == 3) {
            costoTotal += impuesto * 0.10;
        } else if (numEjes > 3) {
            costoTotal += impuesto * 0.15;
        }

        System.out.println("El costo del vehículo es: " + costoTotal);
        //10000 costo 
        //    2 pasajeros 
        //    3 ejes
        //10111 Total
         

        double nota = Double.parseDouble(JOptionPane.showInputDialog("Digite la nota"));

        if (nota >= 90) {
            System.out.println("Sobresaliente");
        } else if (nota >= 80) {
            System.out.println("Notable");
        } else if (nota >= 70) {
            System.out.println("Bien");
        } else {
            System.out.println("Insuficiente");
        }
         

        int cantCan = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de canciones"));
        int cantPar = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de partituras"));

        if (cantCan >= 7 && cantCan <= 10) {
            if (cantPar == 0) {
                System.out.println("Músico naciente");
            } else if (cantPar >= 1 && cantPar <= 5) {
                System.out.println("Músico estelar");
            }
        } else if (cantCan > 10 && cantPar > 5) {
            System.out.println("Músico consagrado");
        } else {
            System.out.println("Músico en formación");
        }
       

        int monto = Integer.parseInt(JOptionPane.showInputDialog("Digite el monto a retirar"));

        int monedas = monto / 500;
        monto %= 500;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 500");
        }
        
        monedas = monto / 200;
        monto %= 200;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 200");
        }
        
        monedas = monto / 100;
        monto %= 100;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 100");
        }
        
        monedas = monto / 50;
        monto %= 50;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 50");
        }
        monedas = monto / 20;
        monto %= 20;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 20");
        }
        monedas = monto / 10;
        monto %= 10;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 10");
        }
        monedas = monto / 5;
        monto %= 5;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 5");
        }
        monedas = monto / 2;
        monto %= 2;
        if (monedas > 0) {
            System.out.println(monedas + " monedas de 2");
        }
        
        if (monto > 0) {
            System.out.println(monedas + " monedas de 1");
        }
        
    }

}
