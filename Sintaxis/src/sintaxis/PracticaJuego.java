/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class PracticaJuego {

    public static int leerInt(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        int n2 = sc.nextInt();
        return n2;
    }

    public static void main(String[] args) {
        int aleatorio = (int) (Math.random() * 15) + 1;
        System.out.println(aleatorio);
        //Intentos 
        for (int i = 1; i <= 5; i++) {
            int num = leerInt("Digite el número" + i + "/5");
            if (num == aleatorio) {
                System.out.println("Ganaste!!!!");
                break;
            } else if (i == 5) {
                System.out.println("Perdiste, se acabaron los intentos");
            } else if (num > aleatorio) {
                System.out.println("El número a adivinar es menor");
            } else {
                System.out.println("El número a adivinar es mayor");
            }
        }
    }
}
