/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraSwitch {

    public static int leerInt(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        int n2 = sc.nextInt();
        return n2;
    }
    
    public static String leerTexto(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        String n2 = sc.nextLine();
        return n2;
    }

    public static void main(String[] args) {
        int dia = leerInt("Digite el día de la semana");
        System.out.println(dia);

        switch (dia) {
            case 1:
                System.out.println("Domingo");
                break;
            case 2:
                System.out.println("Lunes");
                break;
            case 3:
                System.out.println("Martes");
                break;
            case 4:
                System.out.println("Miércoles");
                break;
            case 5:
                System.out.println("Jueves");
                break;
            case 6:
                System.out.println("Viernes");
                break;
            case 7:
                System.out.println("Sábado");
                break;
            default:
                System.out.println("Valor inválido, digite un número entre 1 - 7");
        }

        if (dia == 1) {
            System.out.println("Domingo");
        } else if (dia == 2) {
            System.out.println("Lunes");
        } else if (dia == 3) {
            System.out.println("Martes");
        } else if (dia == 4) {
            System.out.println("Miércoles");
        } else {
            System.out.println("Valor inválido, digite un número entre 1 - 7");
        }

        int mes = leerInt("Digite el mes");
        switch (mes) {
            case 1:
                System.out.println("Enero");
                break;
            case 2:
                System.out.println("Febrero");
                break;
            case 3:
                System.out.println("Marzo");
                break;
            case 4:
                System.out.println("Abril");
                break;
            case 5:
                System.out.println("Mayo");
                break;
            case 6:
                System.out.println("Junio");
                break;
            case 7:
                System.out.println("Julio");
                break;
            case 8:
                System.out.println("Agosto");
                break;
            case 9:
                System.out.println("Setiembre");
                break;
            case 10:
                System.out.println("Octubre");
                break;
            case 11:
                System.out.println("Noviembre");
                break;
            case 12:
                System.out.println("Diciembre");
                break;

        }

        int filas = leerInt("Digite al cantidad de filas");

        switch (filas) {
            case 5:
                System.out.println("************");
            case 4:
                System.out.println("************");
                filas = 2;
            case 3:
                System.out.println("************");
            case 2:
                System.out.println("************");
            case 1:
                System.out.println("************");
        }

        int numero = leerInt("Digite un número entre 0 y 99");

        int unidades = numero % 10;
        int decenas = numero / 10;

        System.out.println(unidades);
        System.out.println(decenas);

        switch (decenas) {
            case 0:
                break;
            case 1:
                switch(unidades){
                    case 0: 
                        System.out.println("diez");
                        break;
                    case 1: 
                        System.out.println("once");
                        break;
                    case 2: 
                        System.out.println("doce");
                        break;
                    case 3: 
                        System.out.println("trece");
                        break;
                    case 4: 
                        System.out.println("catorce");
                        break;
                    case 5: 
                        System.out.println("quince");
                        break;
                    case 6: 
                        System.out.println("dieciseis");
                        break;
                    case 7: 
                        System.out.println("diecisiete");
                        break;
                    case 8: 
                        System.out.println("dieciocho");
                        break;
                    case 9: 
                        System.out.println("diecinueve");
                        break;
                }
                unidades = -1;
                break;
            case 2:
                System.out.print("veinte");
                break;
            case 3:
                System.out.print("treinta");
                break;
            case 4:
                System.out.print("cuarenta");
                break;
            case 5:
                System.out.print("cincuenta");
                break;
            case 6:
                System.out.print("sesenta");
                break;
            case 7:
                System.out.print("setenta");
                break;
            case 8:
                System.out.print("ochenta");
                break;
            case 9:
                System.out.print("noventa");
                break;
        }

        if (decenas > 0 && unidades > 0) {
            System.out.print(" y ");
        }

        switch (unidades) {
            case 0: 
                System.out.println("cero");
                break;
            case 1:
                System.out.println("uno");
                break;
            case 2:
                System.out.println("dos");
                break;
            case 3:
                System.out.println("tres");
                break;
            case 4:
                System.out.println("cuatro");
                break;
            case 5:
                System.out.println("cinco");
                break;
            case 6:
                System.out.println("seis");
                break;
            case 7:
                System.out.println("siete");
                break;
            case 8:
                System.out.println("ocho");
                break;
            case 9:
                System.out.println("nueve");
                break;
        }
        
        
        String letra = leerTexto("Digite la nota letra entre a - e");
        switch(letra){
            case "A":
            case "a":
                System.out.println("Excelente");
                break;
            case "B":
            case "b":
                System.out.println("Bueno");
                break;
            case "C":
            case "c":
                System.out.println("Regular");
                break;
            case "D":
            case "d":
                System.out.println("Malo");
                break;
            case "E":
            case "e":
                System.out.println("Pesimo");
                break;
        }
        
        
        
        
    }
}
