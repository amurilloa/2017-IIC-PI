/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

/**
 *
 * @author ALLAN
 */
public class EstructuraArreglos {

    public static void main(String[] args) {

        Logica log = new Logica();
        int[] datos = {1, 2, 3, 4, 5, 6};
        System.out.println("Promedio: " + log.promedio(datos));
        System.out.println("Mayor: " + log.buscarMayor(datos));
        System.out.println("Suma de Impares: " + log.sumaImpares(datos));
        System.out.println("Buscar Elemento (5): " + log.buscarElemento(datos, 5));
        System.out.println("Buscar Elemento (7): " + log.buscarElemento(datos, 7));
        int pos = log.buscarPos(datos, 6);
        System.out.println("Buscar Posición (6): " + (pos == -1 ? "No se encontró" : pos));
        System.out.println("Original: " + log.imprimir(datos));
        System.out.println("Invertido: "+ log.imprimir(log.invertir(datos)));
        log.rotar(datos);
        System.out.println("Rotar: "+ log.imprimir(datos));
        log.rotar(datos);
        System.out.println("Rotar: "+ log.imprimir(datos));
        log.rotar(datos);
        System.out.println("Rotar: "+ log.imprimir(datos));
        
        // Paso de paráemtros por valor y paso de parámetros por referencia
        
//        int[] arreglo = new int[5]; // Declarado e inicializado
//        arreglo[2] = 10;
//        arreglo[4] = -4;
//        arreglo[arreglo.length-1] = 8;
//        System.out.println(arreglo.length);
//            
//        System.out.println("Como imprimir un arrelgo");
//        for (int i = 0; i < arreglo.length; i++) {
//            System.out.println(arreglo[i]);
//        }
//        
//        Persona p = new Persona(206470762, "Allan", "Murillo");
//        //System.out.println(p);
//        
//        Persona[] personas = new Persona[10];
//        personas[0] = new Persona(206520946, "Lineth", "Matamoros");
//        personas[4] = p;
//        for (int i = 0; i < personas.length; i++) {
//            System.out.println(personas[i]);
//        }
//        
//        String[] nombres = new String[10];
//        nombres[0] = "Sara";
    }
}
