/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.arreglos;

/**
 *
 * @author ALLAN
 */
public class Terminal {

    public String rutas(int monto, Ciudad[] ciudades) {
        String mensaje = "";

        for (int i = 0; i < ciudades.length; i++) {
            if (ciudades[i].getTarifa() <= monto) {
                mensaje += ciudades[i].getNombre() + ", ";
            }
        }
        mensaje +="\b\b";
        return mensaje;
    }
    
    public Ciudad mayorTarifa(Ciudad[] ciudades){
        Ciudad ciudad = ciudades[0];
        for (int i = 0; i < ciudades.length; i++) {
            if(ciudades[i].getTarifa()>ciudad.getTarifa()){
                ciudad = ciudades[i];
            }
        }
        return ciudad;
    }

}
