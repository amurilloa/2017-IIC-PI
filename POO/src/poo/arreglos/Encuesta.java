/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.arreglos;

import java.util.Random;

/**
 *
 * @author ALLAN
 */
public class Encuesta {

    public Estudiante[] generarDatos() {
        Random r = new Random();

        int d = 5;//r.nextInt(50) + 1;
        Estudiante[] encuesta = new Estudiante[d];

        for (int i = 0; i < encuesta.length; i++) {
            Estudiante e = new Estudiante();
            e.setCedula(i);
            e.setSexo(r.nextInt(2) + 1);
            e.setTrabajo(r.nextInt(2) + 1);
            if (e.getTrabajo() == 1) {
                e.setSueldo(r.nextInt(250000) + 1);
            }
            encuesta[i] = e;
        }
        return encuesta;
    }

    public double porcentajePorSexo(Estudiante[] encuesta, int sexo) {
        int cantidad = 0;

        for (int i = 0; i < encuesta.length; i++) {
            if (encuesta[i].getSexo() == sexo) {
                cantidad++;
            }
        }
        return cantidad * 100 / encuesta.length;
    }

    public String trabajoSueldoPro(Estudiante[] encuesta, int sexo) {
        String res = "";
        int can = 0;
        int tot = 0;
        int sue = 0;
        for (int i = 0; i < encuesta.length; i++) {
            if (encuesta[i].getSexo() == sexo) {
                tot++;
                if (encuesta[i].getTrabajo() == 1) {
                    can++;
                    sue += encuesta[i].getSueldo();
                }
            }
        }
        res += "Porcentaje de ";
        res += sexo == 1 ? "Hombres " : "Mujeres ";
        res += can * 100 / tot;
        res += "\n Sueldo Promedio: " + (sue / can);
        return res;
    }

    public String imprimirEncuesta(Estudiante[] encuesta) {
        String res = "";
        for (int i = 0; i < encuesta.length; i++) {
            res += encuesta[i] + "\n";
        }
        return res;
    }

}
