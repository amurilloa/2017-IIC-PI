/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.arreglos;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
//        Encuesta e = new Encuesta();
//        Estudiante[] temp = e.generarDatos();
//        System.out.println(e.imprimirEncuesta(temp));
//        System.out.println("El porcentaje de hombres: " + e.porcentajePorSexo(temp,1) + "%");
//        System.out.println("El porcentaje de mujeres: " + e.porcentajePorSexo(temp,2) + "%");
//        System.out.println(e.trabajoSueldoPro(temp,1));
//        System.out.println(e.trabajoSueldoPro(temp,2));

        Ciudad[] c = new Ciudad[10];
        c[0] = new Ciudad("San Carlos",1700);
        c[1] = new Ciudad("San José",2100);
        c[2] = new Ciudad("Limón",2200);
        c[3] = new Ciudad("Fortuna",1200);
        c[4] = new Ciudad("El Carmen",500);
        c[5] = new Ciudad("Los Chiles",1800);
        c[6] = new Ciudad("Chachagua",1100);
        c[7] = new Ciudad("San Martin",400);
        c[8] = new Ciudad("Los Ángeles",900);
        c[9] = new Ciudad("Alajuela",1600);
        Terminal t = new Terminal();
        System.out.println(t.rutas(1000, c));
        System.out.println(t.mayorTarifa(c));
    }
}
