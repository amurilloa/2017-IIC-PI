/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.arreglos;

/**
 *
 * @author ALLAN
 */
public class Estudiante {

    private int cedula;
    private int sexo;
    private int trabajo;
    private int sueldo;

    public Estudiante() {
    }

    public Estudiante(int cedula, int sexo, int trabajo, int sueldo) {
        this.cedula = cedula;
        this.sexo = sexo;
        this.trabajo = trabajo;
        this.sueldo = sueldo;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public int getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(int trabajo) {
        this.trabajo = trabajo;
    }

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "cedula=" + cedula + ", sexo=" + sexo + ", trabajo=" + trabajo + ", sueldo=" + sueldo + '}';
    }

}
