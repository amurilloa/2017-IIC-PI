/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.persona;

/**
 *
 * @author ALLAN
 */
public class Persona {

    public final char SEX_DEF = 'H';
    public final int IMC_IDEAL = 0;
    public final int IMC_BAJO = -1;
    public final int IMC_SOBRE = 1;

    private String cedula;
    private String nombre;
    private int edad; //Debería ser la fecha de nacimiento
    private char sexo;
    private float peso;
    private float altura;

    public Persona() {
        //this("",0,'H',0f,0f);
        generarCedulaDos();
        nombre = "";
        edad = 0;
        sexo = SEX_DEF;
        peso = 0f;
        altura = 0f;
    }

    public Persona(String nombre, int edad, char sexo) {
        this(nombre, edad, sexo, 0f, 0f);
//        generarCedulaDos();
//        this.nombre = nombre;
//        this.edad = edad;
//        this.sexo = sexo;
//        peso = 0f;
//        altura = 0f;
    }

    public Persona(String nombre, int edad, char sexo, float peso, float altura) {
        //this.cedula = generarCedula();
        generarCedulaDos();
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        comprobarSexo();
        this.peso = peso;
        this.altura = altura;
    }

    /**
     *
     * @return
     */
    private String generarCedula() {
        String ced = "";
        ced += (int) (Math.random() * 6) + 1;
        ced += "-" + (int) (Math.random() * 9999);
        ced += "-" + (int) (Math.random() * 9999);
        return ced;
    }

    /**
     *
     */
    private void generarCedulaDos() {
        cedula = "";
        cedula += (int) (Math.random() * 6) + 1;
        cedula += "-" + (int) (Math.random() * 9999);
        cedula += "-" + (int) (Math.random() * 9999);
    }

    /**
     *
     */
    private void comprobarSexo() {
        if (sexo != 'H' && sexo != 'F') {
            sexo = SEX_DEF;
        }
    }

    /**
     *
     * @return
     */
    public boolean esMayorEdad() {
        return edad >= 18;
    }

    /**
     * Calcula el IMC
     *
     * @return -1: Bajo Peso, 0: Peso Ideal, 1: Sobrepeso
     */
    public int calcularIMC() {
        double imc = peso / Math.pow(altura, 2);
        if (imc < 20) {
            return IMC_BAJO;
        } else if (imc > 25) {
            return IMC_SOBRE;
        } else {
            return IMC_IDEAL;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public String getCedula() {
        return cedula;
    }

    @Override
    public String toString() {
        String sex = "";
        if (sexo == SEX_DEF) {
            sex = "Masculino";
        } else {
            sex = "Femenino";
        }

        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", edad=" + edad + ", sexo=" + sex   + ", peso=" + peso + ", altura=" + altura + '}';
    }
}
