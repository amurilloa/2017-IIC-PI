/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

//Definicion de la clase: Siempre mayuscula y singular (representa 
// un objeto, no varios)
//Atributos: Siempre en minuscula(camelCase) y privados
//Constructores
//Métodos publicos o privados(solo los puede usar dentro de la clase) 
//de la clase 
//Getters y Setters
//toString
//JavaDoc

/**
 *
 * @author ALLAN
 */
public class Garrobo {

    //Atributos 
    private String nombre;
    private double distancia;
    private double tiempo;

    //Constructor
    public Garrobo() {
    }

    public Garrobo(String nombre, double distancia, double tiempo) {
        this.nombre = nombre;
        this.distancia = distancia;
        this.tiempo = tiempo;
    }

    /**
     * Objetivo del método: Calcula el tiempo que tarda el garrobo, en recorrer
     * una distancia dada por parámetro, a partir de la velocidad configurada.
     *
     * @param distancia Tipo double de la distancia que se desea recorrer
     * @return Tipo double con el tiempo que dura reccoriendo la distancia
     */
    public double calcularTiempo(double distancia) {
        double vel = this.distancia / tiempo;
        double tie = distancia / vel;
        return tie;
    }

    public double calcularDistancia(double tiempo) {
        double vel = this.distancia / this.tiempo;
        return vel * tiempo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public String toString() {
        return "Garrobo{" + "nombre=" + nombre + ", distancia=" + distancia + ", tiempo=" + tiempo + '}';
    }
}
