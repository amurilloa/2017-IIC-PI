package listasenlazadas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ALLAN
 */
public class Nodo {
    
    private String dato;
    private Nodo sig;

    public Nodo() {
    }

    public Nodo(String dato) {
        this.dato = dato;
        this.sig = null;
    }

    public Nodo(String dato, Nodo sig) {
        this.dato = dato;
        this.sig = sig;
    }
    
        
    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public Nodo getSig() {
        return sig;
    }

    public void setSig(Nodo sig) {
        this.sig = sig;
    }

    @Override
    public String toString() {
        return "Nodo{" + "dato=" + dato + ", sig=" + sig + '}';
    }
    
    
}
