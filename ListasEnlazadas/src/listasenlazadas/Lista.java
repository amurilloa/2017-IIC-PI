/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadas;

/**
 *
 * @author ALLAN
 */
public class Lista {

    private Nodo primero;

    /**
     * Obtiene el primer elemento encontrado
     *
     * @param nodo Nodo a buscar
     * @return Indice del nodo a buscar
     */
    public int obtenerPos(Nodo nodo) {
        int con = 0;
        Nodo aux = primero;
        while (aux != null) {
            if (nodo.getDato().equals(aux.getDato())) {
                return con;
            }
            con++;
            aux = aux.getSig();
        }
        return -1;
    }

    public int obtenerPos(Nodo nodo, int pos) {
        int con = 0;
        Nodo aux = primero;
        while (aux != null) {
            if (con >= pos && nodo.getDato().equals(aux.getDato())) {
                return con;
            }
            con++;
            aux = aux.getSig();
        }
        return -1;
    }
    //Obtener la posicion de un valor, de x en adelante

    //"hola mundo".indexOf("o") //1
    //"hola mundo".indexOf("o",2) //9
    //Obtener el último
    public Nodo ultimo() {
        if (estaVacia()) {
            return null;
        } else {
            Nodo aux = primero;
            while (aux.getSig() != null) {
                aux = aux.getSig();
            }
            return aux;
        }
    }

    /**
     * Obtiene un nodo en una posicion dada.
     *
     * @param pos int con el indice a buscar
     * @return Nodo con los datos solicitados
     */
    public Nodo obtener(int pos) {
        int con = 0;
        Nodo aux = primero;
        while (aux != null) {
            if (pos == con) {
                return aux;
            }
            con++;
            aux = aux.getSig();
        }
        return null;
    }

    /**
     * Tamaño de la lista
     *
     * @return int con el largo de la lista
     */
    public int tamanno() {
        int tam = 0;

        Nodo aux = primero;
        while (aux != null) {
            tam++;
            aux = aux.getSig();
        }
        return tam;
    }
    //Invertir una lista

    /**
     * Inserta un elmento al final de la lista
     *
     * @param dato String con el dato a almacenar
     */
    public void insertar(String dato) {
        if (estaVacia()) {
            primero = new Nodo(dato);
        } else {
            // 1
            // Nodo aux = primero;
            // while (aux.getSig() != null) {
            //    aux = aux.getSig();
            //  }
            //  aux.setSig(new Nodo(dato));

            // 2
            // Nodo ultimo = ultimo();
            // ultimo.setSig(new Nodo(dato));
            // 3
            ultimo().setSig(new Nodo(dato));
        }
    }
    
    //Insertar al incio, al final, en una posicion
    public void insertarI(String dato){
        Nodo nuevo = new Nodo(dato);
        nuevo.setSig(primero);
        primero = nuevo;
    }

    /**
     * Si la lista esta vacía
     *
     * @return True si la lista esta vacía
     */
    public boolean estaVacia() {
        return primero == null;
    }

    /**
     * Obtener el primer elemento de la lista
     *
     * @return Primer Nodo de la lista
     */
    public Nodo getPrimero() {
        return primero;
    }

}
//Tiempo: 1:30
//1. Reemplazar dato en una posicion
//2. Eliminar el primero, el ultimo, en una posicion
//3. Insertar Ordernado

    