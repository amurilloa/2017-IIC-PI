/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicsfiguras;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author ALLAN
 */
public class Cuadrado {

    private int x;
    private int y;
    private Color color;

    public final int LADO = 60;
    private final int C_LADOS = 4;

    public Cuadrado() {
    }

    public Cuadrado(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public void mover(int tecla) {
        switch (tecla) {
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Cuadrado{" + "x=" + x + ", y=" + y + ", color=" + color + ", LADO=" + LADO + ", C_LADOS=" + C_LADOS + '}';
    }
}
