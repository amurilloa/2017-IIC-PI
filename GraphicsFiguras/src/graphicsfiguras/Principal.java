/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicsfiguras;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class Principal extends JPanel implements KeyListener {

    private Cuadrado cuadrado;
    private Rombo rombo;
    private Triangulo triangulo;
    private int figura;

    public Principal() {
        cuadrado = new Cuadrado(150, 150, Color.RED);
        rombo = new Rombo(450, 150, Color.ORANGE);
        triangulo = new Triangulo(450, 300, Color.YELLOW);
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        // Pintar el fondo
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 600, 600);
        //Cuadrado
        g.setColor(cuadrado.getColor());
        g.fillRect(cuadrado.getX(), cuadrado.getY(), cuadrado.LADO, cuadrado.LADO);

        //Rombo
        g.setColor(rombo.getColor());
        g.fillPolygon(rombo.arregloX(), rombo.arregloY(), rombo.C_LADOS);

        //Triangulo
        g.setColor(triangulo.getColor());
        g.fillPolygon(triangulo.arregloX(), triangulo.arregloY(), triangulo.C_LADOS);
    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {
        //Seleccionar la figura
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_C:
                figura = 0;
                break;
            case KeyEvent.VK_T:
                figura = 1;
                break;
            case KeyEvent.VK_R:
                figura = 2;
                break;
            default:
                break;
        }
        if (ke.getKeyCode() >= 54 && ke.getKeyCode() <= 57 || ke.getKeyCode() == 48) {
            cambiarColor(ke.getKeyCode());
        }

        switch (figura) {
            case 0:
                cuadrado.mover(ke.getKeyCode());
                break;
            case 1:
                triangulo.mover(ke.getKeyCode());
                break;
            case 2:
                rombo.mover(ke.getKeyCode());
                break;
        }

        repaint();
    }

    private void cambiarColor(int tecla) {
        Color temp = Color.WHITE;
        switch (tecla) {
            case KeyEvent.VK_0:
                temp = Color.RED;
                break;
            case KeyEvent.VK_6:
                temp = Color.YELLOW;
                break;
            case KeyEvent.VK_7:
                temp = Color.BLUE;
                break;
            case KeyEvent.VK_8:
                temp = Color.GREEN;
                break;
            case KeyEvent.VK_9:
                temp = Color.PINK;
                break;
        }
        switch (figura) {
            case 0:
                cuadrado.setColor(temp);
                break;
            case 1:
                triangulo.setColor(temp);
                break;
            case 2:
                rombo.setColor(temp);
                break;
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
