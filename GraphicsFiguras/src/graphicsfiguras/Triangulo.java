/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicsfiguras;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author ALLAN
 */
public class Triangulo {

    private int x;
    private int y;
    private Color color;

    private final int BASE = 68;
    private final int ALTURA = 60;

    public final int C_LADOS = 3;

    public Triangulo() {
    }

    public Triangulo(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }
    
    public void mover(int tecla) {
        switch (tecla) {
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }
    }

    public int[] arregloX() {
        int[] res = new int[C_LADOS];
        res[0] = x;
        res[1] = x + BASE / 2;
        res[2] = x + BASE;
        return res;
    }

    public int[] arregloY() {
        int[] res = new int[C_LADOS];
        res[0] = y;
        res[1] = y - ALTURA;
        res[2] = y;
        return res;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Triangulo{" + "x=" + x + ", y=" + y + ", color=" + color + ", BASE=" + BASE + ", ALTURA=" + ALTURA + ", C_LADOS=" + C_LADOS + '}';
    }
}
