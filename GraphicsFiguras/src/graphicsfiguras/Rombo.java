/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicsfiguras;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author ALLAN
 */
public class Rombo {

    private int x;
    private int y;
    private Color color;

    private final int D_MAYOR = 75;
    private final int D_MENOR = 75;
    public final int C_LADOS = 4;

    public Rombo() {
    }

    public Rombo(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }
    
    public void mover(int tecla) {
        switch (tecla) {
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }
    }

    public int[] arregloX() {
        int[] res = new int[C_LADOS];
        res[0] = x;
        res[1] = x + D_MENOR / 2;
        res[2] = x + D_MENOR;
        res[3] = x + D_MENOR / 2;
        return res;
    }

    public int[] arregloY() {
        int[] res = new int[C_LADOS];
        res[0] = y;
        res[1] = y - D_MAYOR / 2;
        res[2] = y;
        res[3] = y + D_MAYOR / 2;
        return res;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Rombo{" + "color=" + color + ", x=" + x + ", y=" + y + ", D_MAYOR=" + D_MAYOR + ", D_MENOR=" + D_MENOR + ", C_LADOS=" + C_LADOS + '}';
    }

}
