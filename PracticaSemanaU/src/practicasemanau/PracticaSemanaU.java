/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicasemanau;

/**
 *
 * @author ALLAN
 */
public class PracticaSemanaU {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Circunferencia rueda = new Circunferencia();
        Circunferencia moneda = new Circunferencia();
        rueda.setRadio(10.2);
        moneda.setRadio(1.4);
        System.out.println("El área de la rueda es: " + rueda.calcularArea());
        System.out.println("El área de la moneda es: " + moneda.calcularArea());
        System.out.println("El perimetro de la rueda es: " + rueda.calcularPerimetro());
        System.out.println("El perimetro de la moneda es: " + moneda.calcularPerimetro());

        Rectangulo pared = new Rectangulo(5, 4);
        pared.setAncho(6);
        System.out.println("El área de la pared es: " + pared.calcularArea());
        double tiempo = pared.calcularArea() * 10;
        System.out.println("El tiempo aprox. es: " + ((int)tiempo/60) + "hrs " + (int)tiempo%60 + "min" );

    }

}
