/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimientographics;

import javax.swing.JFrame;

/**
 *
 * @author ALLAN
 */
public class MovimientoGraphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame frm = new JFrame();
        Principal panel = new Principal();
        frm.add(panel);
        frm.setSize(600, 600);
        frm.setVisible(true);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setLocationRelativeTo(null);
        while (true) {
            frm.repaint();
            Thread.sleep(20);
        }
    }

}
