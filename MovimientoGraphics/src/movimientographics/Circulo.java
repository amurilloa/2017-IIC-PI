/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimientographics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

/**
 *
 * @author ALLAN
 */
public class Circulo {

    private static final int DIAMETRO = 25;
    private int x;
    private int y;
    private int speedX;
    private int speedY;
    private Color color;
    private boolean rebotar;
    private boolean rotar;

    public Circulo() {
        this.speedX = 0;
        this.speedY = 0;
    }

    public Circulo(Color circulo, int x, int y, boolean rebotar, boolean rotar) {
        this.color = circulo;
        this.x = x;
        this.y = y;
        this.speedX = 0;
        this.speedY = 0;
        this.rebotar = rebotar;
        this.rotar = rotar;
    }

    public void mover(int keyChar) {
        switch (keyChar) {
            case KeyEvent.VK_UP:
                if (speedY <= 0) {
                    speedY--;
                } else {
                    speedY *= -1;
                }
                break;
            case KeyEvent.VK_DOWN:
                if (speedY >= 0) {
                    speedY++;
                } else {
                    speedY *= -1;
                }
                break;
            case KeyEvent.VK_LEFT:
                if (speedX <= 0) {
                    speedX--;
                } else {
                    speedX *= -1;
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (speedX >= 0) {
                    speedX++;
                } else {
                    speedX *= -1;
                }
                break;
            case KeyEvent.VK_S:
                speedX = 0;
                speedY = 0;
                break;
        }
    }

    public void mover(int ancho, int alto) {

        x += speedX;
        y += speedY;

        if (rebotar) {
            if (x <= 0 && speedX < 0) {
                speedX *= -1;
            }
            if (x >= ancho - DIAMETRO && speedX > 0) {
                speedX *= -1;
            }
        }
        if (rotar) {
            if (y >= alto + DIAMETRO && speedY > 0) {
                y = 0 - DIAMETRO;
            }

            if (y <= 0 - DIAMETRO && speedY < 0) {
                y = alto;
            }
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circulo{" + "color=" + color + ", x=" + x + ", y=" + y + '}';
    }

    public void paint(Graphics2D g) {
        g.setColor(color);
        g.fillOval(x, y, DIAMETRO, DIAMETRO);
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, DIAMETRO, DIAMETRO);
    }

    public void choque(Raqueta j1, Raqueta j2) {
        if (getBounds().intersects(j1.getBounds(1))) {
            mover(KeyEvent.VK_DOWN);
            mover(KeyEvent.VK_LEFT);
        }else if (getBounds().intersects(j1.getBounds(2))) {
            mover(KeyEvent.VK_DOWN);
        }else if (getBounds().intersects(j1.getBounds(3))) {
            mover(KeyEvent.VK_DOWN);
            mover(KeyEvent.VK_RIGHT);
        }
        
        if (getBounds().intersects(j2.getBounds(1))) {
            mover(KeyEvent.VK_UP);
            mover(KeyEvent.VK_LEFT);
        }else if (getBounds().intersects(j2.getBounds(2))) {
            mover(KeyEvent.VK_UP);
        }else if (getBounds().intersects(j2.getBounds(3))) {
            mover(KeyEvent.VK_UP);
            mover(KeyEvent.VK_RIGHT);
        }
    }
}
