/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenallanmurillo;

/**
 *
 * @author ALLAN
 */
public class ExamenAllanMurillo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Instancia de la logica
        Logica log = new Logica();
        //Ejercicio 1 
        int[] arreglo = log.generarArreglo(10);
        System.out.println("Arreglo Original: " + log.imprimir(arreglo));
        //Ejercicio 2
        //Punto a
        System.out.println("Suma de Impares: " + log.sumarImp(arreglo));
        System.out.println("Números Primos: " + log.numerosPrimos(arreglo));
        System.out.println("Promedio: " + log.promedio(arreglo));
        System.out.print("Reemplazando... : ");
        log.reemplazar(5, -1, arreglo);
        System.out.println(log.imprimir(arreglo));

        System.out.println("\nBilletes - Ejercicio 3");
        Billete[] billetes = log.generarBilletes();
        System.out.println(log.imprimirB(billetes));
        log.depositarB(5000, 5, billetes);
        System.out.println("Retiro: 28000\n" + log.retirarB(28000, billetes));
        System.out.println("Retiro: 14000\n" + log.retirarB(14000, billetes));
        System.out.println("Retiro: 19000\n" + log.retirarB(19000, billetes));
        System.out.println("Retiro: 10000\n" + log.retirarB(10000, billetes));
        System.out.println("Retiro: 3000\n" + log.retirarB(3000, billetes));

        System.out.println("EXTRA - Guardando Billetes");
        log.guardarBilletes(billetes);
        System.out.println(log.imprimirB(billetes));

    }

}
