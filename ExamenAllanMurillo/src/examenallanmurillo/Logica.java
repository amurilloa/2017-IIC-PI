/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Genera un arreglo de una dimensión N con números aleatorios
     *
     * @param dimension dimension del arreglo
     * @return arreglo con números aleatorios
     */
    public int[] generarArreglo(int dimension) {
        int[] temp = new int[dimension];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = (int) (Math.random() * 10) + 1;
        }
        return temp;
    }

    /**
     * Pasa a String (con un formato) un arreglo de enteros
     *
     * @param arreglo Arreglo que se pasa a String
     * @return String con los datos del arreglo
     */
    public String imprimir(int[] arreglo) {
        String res = "";
        for (int i = 0; i < arreglo.length; i++) {
            res += arreglo[i] + ", ";
        }
        res += "\b\b";
        return res;
    }

    /**
     * Suma los numeros impares de una arreglo
     *
     * @param arreglo Arreglo de donde se obtendran los numeros
     * @return double con el resultado de la suma de impares
     */
    public int sumarImp(int[] arreglo) {
        int total = 0;
        for (int i = 0; i < 10; i++) {
            if (arreglo[i] % 2 != 0) {
                total += arreglo[i];
            }
        }
        return total;
    }

    /**
     * Obtiene los números primos de un arreglo
     *
     * @param arreglo arreglo con los datos
     * @return String con los números primos del arreglo concatenados ,
     * separados por una ','
     */
    public String numerosPrimos(int[] arreglo) {
        String res = "";
        for (int i = 0; i < arreglo.length; i++) {
            if (esPrimo(arreglo[i])) {
                res += arreglo[i] + ", ";
            }
        }

        res += "\b\b";
        return res;
    }

    /**
     * Determina si un número es primo o no
     *
     * @param numero Número a evaluar
     * @return true si el número es primo
     */
    public boolean esPrimo(int numero) {
        int con = 1;
        int div = 0;
        while (con <= numero) {
            if (numero % con == 0) {
                div++;
            }
            if (div > 2) {
                return false;
            }
            con++;
        }
        if (div == 2) {
            return true;
        }
        return false;
    }

    /**
     * Obtiene el promdio de los elementos de un arreglo
     *
     * @param arreglo arreglo con los datos
     * @return double con el promedio
     */
    public double promedio(int[] arreglo) {
        double res = 0;
        for (int i = 0; i < arreglo.length; i++) {
            res += arreglo[i];
        }
        return res / arreglo.length;
    }

    /**
     * Reemplaza los elementos en el arreglo
     *
     * @param num1 Numero a reemplazar
     * @param num2 Número nuefo
     * @param arreglo Arreglo con los datos
     */
    public void reemplazar(int num1, int num2, int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == num1) {
                arreglo[i] = num2;
            }
        }
    }

    /**
     * Metodo para generar los billetes
     *
     * @return Arreglo de billetes
     */
    public Billete[] generarBilletes() {
        Billete[] temp = new Billete[5];
        temp[4] = new Billete(1000, 15);
        temp[3] = new Billete(2000, 10);
        temp[2] = new Billete(5000, 6);
        temp[1] = new Billete(10000, 3);
        temp[0] = new Billete(20000, 1);
        return temp;
    }

    /**
     * Pasa a String (con un formato) un arreglo de billetes
     *
     * @param arreglo Arreglo que se pasa a String
     * @return String con los datos del arreglo
     */
    public String imprimirB(Billete[] arreglo) {
        String res = "";
        for (int i = 0; i < arreglo.length; i++) {
            res += arreglo[i].getCantidad() + ": " + arreglo[i].getValor() + "\n";
        }
        res += "\b\b";
        return res;
    }

    /**
     * Deposita una cantidad de billetes en un arreglo
     *
     * @param valor Valor del billete 1000, 5000, 2000, 10000, etc
     * @param cantidad Cantidad de billetes a depositar
     * @param arreglo Arreglo donde se van a depositar los billetes
     */
    public void depositarB(int valor, int cantidad, Billete[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] != null && arreglo[i].getValor() == valor) {
                arreglo[i].setCantidad(arreglo[i].getCantidad() + cantidad);
            }
        }
    }

    /**
     * Retira un monto determinado de un arreglo de billetes
     *
     * @param monto Monto a retirar
     * @param billetes Arreglo de billetes de donde se sacará el dinero
     * @return String con el desglose de billetes dispensados
     */
    public String retirarB(int monto, Billete[] billetes) {
        String res = "";
        int saldo = monto;
        for (int i = 0; i < billetes.length; i++) {
            int cant = saldo / billetes[i].getValor();
            if (cant > 0) {
                if (cant > billetes[i].getCantidad()) {
                    saldo -= billetes[i].getCantidad() * billetes[i].getValor();
                    res += billetes[i].getCantidad() + ": " + billetes[i].getValor() + "\n";
                    billetes[i].setCantidad(0);
                } else {
                    saldo -= cant * billetes[i].getValor();
                    res += cant + ": " + billetes[i].getValor() + "\n";
                    billetes[i].setCantidad(billetes[i].getCantidad() - cant);
                }
            }
            if (saldo <= 0) {
                break;
            }
        }
        return res;
    }

    /**
     * Guarda el estado del arreglo de billtes en un archivo de texto
     *
     * @param billetes Arreglo de billetes que se desea guardar
     */
    public void guardarBilletes(Billete[] billetes) {
        String res = "";
        for (int i = 0; i < billetes.length; i++) {
            res += billetes[i].getValor() + "," + billetes[i].getCantidad() + "\n";
        }
        ManejadorArchivos arch = new ManejadorArchivos();
        arch.escribirTextoArchivo("Billetes.txt", res);
    }

}
