/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Billete {
    
    private int valor;
    private int cantidad;

    public Billete() {
    }

    public Billete(int valor, int cantidad) {
        this.valor = valor;
        this.cantidad = cantidad;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Billete{" + "valor=" + valor + ", cantidad=" + cantidad + '}';
    }
    
}
