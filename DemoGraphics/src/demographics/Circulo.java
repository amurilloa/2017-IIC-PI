/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author ALLAN
 */
public class Circulo {

    private Color color;
    private int x;
    private int y;

    public Circulo() {
    }

    public Circulo(Color circulo, int x, int y) {
        this.color = circulo;
        this.x = x;
        this.y = y;
    }

    public void mover(int keyChar) {
        switch (keyChar) {
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circulo{" + "color=" + color + ", x=" + x + ", y=" + y + '}';
    }

}
