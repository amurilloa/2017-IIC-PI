/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class FondoLineas extends JPanel implements KeyListener {

    private Color fondo;
    LinkedList<Circulo> c;
    
    
    public FondoLineas() {
        c = new LinkedList<Circulo>();
        c.add(new Circulo(Color.BLUE, 100, 100));
        fondo = Color.BLACK;
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //Pinta el fondo
        g.setColor(fondo);
        g.fillRect(0, 0, getWidth(), getHeight());
        //Pinta el circulo 
        for (int i = 0; i < c.size(); i++) {
            Circulo temp = c.get(i);
            g.setColor(temp.getColor());
            g.fillOval(temp.getX(), temp.getY(), 50, 50);

        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        getColor(ke.getKeyChar());
        Circulo t = c.getLast();
        c.add(new Circulo(t.getColor(),t.getX(),t.getY()));
        c.getLast().mover(ke.getKeyCode());
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

    private void getColor(char code) {
        switch (code) {
            case '1':
                fondo = Color.BLUE;
                break;
            case '2':
                fondo = Color.YELLOW;
                break;
            case '3':
                fondo = Color.GREEN;
                break;
            case '4':
                fondo = Color.MAGENTA;
                break;
            case '5':
                fondo = Color.CYAN;
                break;
            case '6':
                c.getLast().setColor(Color.BLUE);
                break;
            case '7':
                c.getLast().setColor(Color.YELLOW);
                break;
            case '8':
                c.getLast().setColor(Color.GREEN);
                break;
            case '9':
                c.getLast().setColor(Color.MAGENTA);
                break;
            case '0':
                c.getLast().setColor(Color.CYAN);
                break;
        }
    }
}
