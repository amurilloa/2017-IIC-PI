/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import javax.swing.JFrame;
;
/**
 *
 * @author ALLAN
 */
public class DemoGraphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Monigote fondo = new Monigote();//Se crea un objeto de tipo panel personalizado
        FondoLineas f = new FondoLineas();
        JFrame frm = new JFrame();// Creamos un formulario
        //frm.add(fondo); //Agregammos un panel al formulario
        frm.add(f);
        frm.setSize(400,400); // Le damos un tamaño a la ventana
        frm.setVisible(true); // Hace visible la ventana
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Accion por defecto al cerrar
        frm.setLocationRelativeTo(null); //Centrar la ventana
    }

}
